package uk.co.deloittedigital.customviews.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * A general Utils class for helper methods related to ensuring things are displayed correctly
 */
public class DisplayUtils {

    /**
     * Converts dp to pixels based on the device's display metrics
     *
     * @param dp
     * @return
     */
    public static int convertDpToPx(int dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    /**
     * Retrieves the data value for an attribute, as determined by the theme.
     *
     * @param viewContext The context that the theme should be retrieved from. If you want a theme
     *                    attribute for a particular view, then this should be the view's context.
     * @param resourceId The ID of the attribute for which you want the data value
     * @return
     */
    public static int getAttributeDataFromCurrentTheme(Context viewContext, int resourceId){
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = viewContext.getTheme();
        theme.resolveAttribute(resourceId, typedValue, true);
        int attributeValue = typedValue.data;
        return attributeValue;
    }
}
