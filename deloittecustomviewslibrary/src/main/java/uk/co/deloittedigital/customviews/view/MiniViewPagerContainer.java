package uk.co.deloittedigital.customviews.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.scopely.fontain.Fontain;

import java.util.LinkedList;
import java.util.List;

import uk.co.deloittedigital.customviews.adapter.MiniViewPagerAdapter;
import uk.co.deloittedigital.customviews.util.CustomLogger;
import uk.co.deloittedigital.customviewslib.R;

/**
 * Custom view to be used for creating the MiniViewPager.
 * Layout consists of a view pager and an image overlay for the circle around the selected item.
 * The view pager and image are wrapped in a frame layout.
 */
public class MiniViewPagerContainer<T> extends FrameLayout {

    private static final String SCHEME = "http://schemas.android.com/apk/res-auto";
    private static final String FONT_ATTRIBUTE = "custom_font_family";

    private CustomLogger mLog;
    // TODO Document how the library user can change minimum text view by declaring their own overriding dimension resource
    private int mMinimumPageWidthDp;
    private FrameLayout mImageOverlayWrapper;
    private MiniViewPager mMiniViewPager;
    private View mImageOverlayForSelectedPage;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<T> mPagerValues;
    private String mCustomFontFamily;

    public MiniViewPagerContainer(Context context) {
        super(context);
        init(context);
    }

    public MiniViewPagerContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        extractAttributes(attrs);
        init(context);
    }

    public MiniViewPagerContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        extractAttributes(attrs);
        init(context);
    }

    private void extractAttributes(AttributeSet attrs){
        mCustomFontFamily = attrs.getAttributeValue(SCHEME, FONT_ATTRIBUTE);
    }

    /**
     * Initialises viewpager container and children
     * @param context The relevant activity's context
     */
    private void init(Context context){
        mLog = new CustomLogger(false, getClass().getSimpleName());
        mContext = context;
        bindViews();

        mMinimumPageWidthDp = (int) mContext.getResources().getDimension(R.dimen.miniviewpager_minimum_page_width);
        mPagerValues = new LinkedList<>();
        createMiniViewPager();
        mMiniViewPager.setAdapter(createAdapter());
    }

    /**
     * Sets the values to be displayed in the MiniViewPager. The toString method of these values
     * will be used to display them.
     *
     * @param values The values to be displayed by the MiniViewPager.
     */
    public void setPagerValues(List values){
        String longestString = determineLongestString(values);
        mMiniViewPager.getCastedAdapter().updateDataSet(values, longestString);
        int widestElementWidthPx = mMiniViewPager.getCastedAdapter().getWidthOfWidestElementPx();
        createImageOverlay(widestElementWidthPx);
        updateViewPagerPadding(widestElementWidthPx);
    }

    /**
     * @return The viewpager being used to display the pages
     */
    public ViewPager getViewPager(){
        return this.mMiniViewPager;
    }

    private void bindViews(){
        mLayoutInflater = LayoutInflater.from(mContext);
        View containerView = mLayoutInflater.inflate(R.layout.miniviewpager_container, this);
        mMiniViewPager = (MiniViewPager) containerView.findViewById(R.id.minipager);
        mImageOverlayWrapper = (FrameLayout) containerView.findViewById(R.id.pagerimageoverlaywrapper);
        mImageOverlayForSelectedPage = mImageOverlayWrapper.findViewById(R.id.miniviewpagerimage);

        if (mCustomFontFamily != null) {
            Fontain.applyFontFamilyToViewHierarchy(containerView, Fontain.getFontFamily(mCustomFontFamily));
        }
    }

    /**
     * Creates the view pager with the required configuration in order for it to display as expected.
     */
    private MiniViewPager createMiniViewPager(){
        mMiniViewPager.setClipToPadding(false);
        int padding = calculatePagerPaddingFromPageWidth(mMinimumPageWidthDp);
        mMiniViewPager.setPadding(padding, 0, padding, 0);
        return mMiniViewPager;
    }

    private MiniViewPagerAdapter createAdapter(){
        MiniViewPagerAdapter adapter = new MiniViewPagerAdapter(mPagerValues, mMiniViewPager);
        if(mCustomFontFamily != null){
            adapter.setCustomFontFamily(mCustomFontFamily);
        }
        return adapter;
    }

    private int calculatePagerPaddingFromPageWidth(int calculatedPageWidth){
        int pageWidthToUse = calculatedPageWidth;
        if(pageWidthToUse < mMinimumPageWidthDp){
            pageWidthToUse = mMinimumPageWidthDp;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round((displayMetrics.widthPixels - pageWidthToUse) / 2);
    }

    /**
     * Creates the image overlay which will display the shape around the selected page to highlight it
     */
    private void createImageOverlay(int widestElementWidthPx){
        mImageOverlayForSelectedPage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.miniviewpager_selected_overlay));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(widestElementWidthPx, getResources().getDimensionPixelSize(R.dimen.miniviewpager_selector_dimen));
        layoutParams.gravity = Gravity.CENTER;
        mImageOverlayForSelectedPage.setLayoutParams(layoutParams);
        mImageOverlayForSelectedPage.invalidate();
    }

    private void updateViewPagerPadding(int widestPageWidth){
        int padding = calculatePagerPaddingFromPageWidth(widestPageWidth);
        mMiniViewPager.setPadding(padding, 0, padding, 0);
    }

    private String determineLongestString(List values){
        String longestString = "";
        for(Object o : values){
            if(o.toString().length() > longestString.length()){
                longestString = o.toString();
            }
        }
        return longestString;
    }

}