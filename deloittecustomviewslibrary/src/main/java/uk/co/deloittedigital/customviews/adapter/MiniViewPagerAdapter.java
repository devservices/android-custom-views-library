package uk.co.deloittedigital.customviews.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scopely.fontain.Fontain;

import java.util.List;

import uk.co.deloittedigital.customviews.util.CustomLogger;
import uk.co.deloittedigital.customviews.util.DisplayUtils;
import uk.co.deloittedigital.customviewslib.R;

/**
 * The PagerAdapter to be used with the MiniViewPager
 * @param <T> the data type to be displayed on each page. The .toString() method of this type will
 *           be used to display it.
 */
public class MiniViewPagerAdapter<T> extends PagerAdapter {

    private static final String TAG = MiniViewPagerAdapter.class.getSimpleName();
    private final LayoutInflater mLayoutInflater;
    private final CustomLogger mLog;
    private final ViewPager mCallingViewPager;
    private Context mContext;
    private List<T> mValues;
    private View.OnTouchListener mItemOnTouchListener;
    private String mLongestString;
    private String mCustomFontFamily;

    /**
     * Initialises the PagerAdapter for the MiniViewPager.
     * @param values The values to be displayed by the adapter
     * @param viewPager The viewpager that this adapter will belong to
     */
    public MiniViewPagerAdapter(List<T> values, ViewPager viewPager){
        mLog = new CustomLogger(true, getClass().getSimpleName());
        mValues = values;
        mCallingViewPager = viewPager;
        mContext = viewPager.getContext();
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public Object instantiateItem(ViewGroup viewpager, int position) {

        View view = mLayoutInflater.inflate(R.layout.miniviewpager_element, viewpager, false);

        TextView tv = (TextView) view;
        tv.setText(mValues.get(position).toString());
        styleText(viewpager, tv, position);
        tv.setTag(position);
        setListenerForItemViewTouch(tv);
        viewpager.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    /**
     * Updates the list of items to be displayed. Calls {@link #notifyDataSetChanged()}.
     *
     * @param dataSet The list of items to be displayed
     * @param longestString The longest string out of those list of items
     */
    public void updateDataSet(List<T> dataSet, String longestString){
        mValues = dataSet;
        mLongestString = longestString;
        notifyDataSetChanged();
    }

    /**
     *  Creates a Rect which encloses the widest element from the adapter's longest string.
     *  The longest string must have already been passed to the adapter through {@link #updateDataSet(List, String)}.
     *
     *  The rect encloses only the longest string to be displayed - it does not include padding or take into
     *  account any minimum width. Consider using {@link #getWidthOfWidestElementPx()} if you need those.
     *
     * @return A rect enclosing the widest element's string
     */
    public Rect getRawBoundsOfWidestElement(){
        if(mLongestString == null){
            mLongestString = "";
        }

        // Appending extra character to resolve issue where last char wasn't accounted for
        mLongestString = mLongestString + "o";
        Rect bounds = new Rect();
        TextView elementView = (TextView) mLayoutInflater.inflate(R.layout.miniviewpager_element, mCallingViewPager, false);
        elementView.getPaint().getTextBounds(mLongestString, 0, mLongestString.length(), bounds);
        return bounds;
    }

    /**
     *  Calculates the width of the widest element's view, taking into account any minimum width
     *  or padding. Makes use of {@link #getRawBoundsOfWidestElement()}.
     *
     * @return Width in pixels of the textview of the widest element
     */
    public int getWidthOfWidestElementPx(){
        Rect bounds = getRawBoundsOfWidestElement();
        int rawWidthPx = bounds.width();

        int minimumWidth = (int) mContext.getResources().getDimension(R.dimen.miniviewpager_minimum_page_width);
        if(rawWidthPx < minimumWidth){
            rawWidthPx = minimumWidth;
        }
        int extraPaddingDp = (int) mContext.getResources().getDimension(R.dimen.miniviewpager_padding_each_side_of_page);
        int extraPaddingPx = DisplayUtils.convertDpToPx(extraPaddingDp);
        return rawWidthPx + extraPaddingPx;
    }

	/**
     * Sets the custom font family user has specified from XML attributes for the parent {@link uk.co.deloittedigital.customviews.view.MiniViewPager}.
     * @param customFontFamily
     */
    public void setCustomFontFamily(String customFontFamily){
        mCustomFontFamily = customFontFamily;
    }

    /**
     * Sets the listener to be used when the user interacts with a page
     * @param listener
     */
    public void setItemTouchListener(View.OnTouchListener listener){
        mItemOnTouchListener = listener;
    }

    /**
     * Sets the color and font of the textview. Assumes that the first in the first position is selected, and
     * therefore should be styled as such.
     * @param rootView The ViewGroup which holds the item.
     * @param tv The TextView for which the color should be set
     * @param position The position that the textview is in in the list of values
     */
    private void styleText(ViewGroup rootView, TextView tv, int position){
        if(position == 0){
            int primaryThemeColor = DisplayUtils.getAttributeDataFromCurrentTheme(tv.getContext(), R.attr.colorPrimary);
            tv.setTextColor(primaryThemeColor);
        }

        if (mCustomFontFamily != null) {
            Fontain.applyFontFamilyToViewHierarchy(rootView, Fontain.getFontFamily(mCustomFontFamily));
        }
    }

    /**
     * Adds this view to the list of views being listened to for user interaction
     * @param itemView The view to set the touchlistener for
     */
    private void setListenerForItemViewTouch(View itemView){
        if(mItemOnTouchListener != null) {
            itemView.setOnTouchListener(mItemOnTouchListener);
        }
    }
}
