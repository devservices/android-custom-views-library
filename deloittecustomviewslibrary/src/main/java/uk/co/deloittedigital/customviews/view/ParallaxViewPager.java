package uk.co.deloittedigital.customviews.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import uk.co.deloittedigital.customviewslib.R;
import uk.co.deloittedigital.customviews.util.CustomLogger;

/**
 * Horizontally oriented Parallax ViewPager
 */
public class ParallaxViewPager extends ViewPager {

    // TODO Image scaling
    private Bitmap mBackgroundBitmap;
    private CustomLogger mLog;
    private Rect src;
    // TODO Document parallaxPercent - the percent of width from each side that can go off screen
    private float mParallaxPercent;
    private int mParallaxPixels;
    private Rect dest = new Rect();

    public ParallaxViewPager(Context context){
        super(context);
        init();
    }

    /**
     * Initialise the Parallax ViewPager
     * @param context
     * @param attrs
     */
    public ParallaxViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray viewAttribubtes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ParallaxViewPager, 0 , 0);
        convertBackgroundResourceToBitmap(viewAttribubtes);
        extractParallaxPercentAttr(viewAttribubtes);
        viewAttribubtes.recycle();
        init();
    }

    @Override
    public void onPageScrolled(int position, float offset, int offsetPixels){
        super.onPageScrolled(position, offset, offsetPixels);
        calculateSrc(position, offset);
    }

    @Override
    public void onDraw(Canvas canvas){
        if(mBackgroundBitmap != null) {
            canvas.getClipBounds(dest);
            canvas.drawBitmap(mBackgroundBitmap, src, dest, null);
        }
    }

    private void init(){
        mLog = new CustomLogger(true, getClass().getSimpleName());
        initSrcRect();
        setOffscreenPageLimit(50);
    }

    private void initSrcRect(){
        mParallaxPixels = (int) ((float) mBackgroundBitmap.getWidth() * (mParallaxPercent / 100));
        int initialRectWidth = mBackgroundBitmap.getWidth() - mParallaxPixels;
        int initialRectHeight = mBackgroundBitmap.getHeight();
        src = new Rect(0, 0, initialRectWidth, initialRectHeight);
    }

    private void convertBackgroundResourceToBitmap(TypedArray viewAttrs){
        int mBackgroundParallaxImageResourceId = viewAttrs.getResourceId(R.styleable.ParallaxViewPager_backgroundParallaxImage, 0);
        mBackgroundBitmap = BitmapFactory.decodeResource(getResources(), mBackgroundParallaxImageResourceId);
    }

    private void extractParallaxPercentAttr(TypedArray viewAttrs){
        mParallaxPercent = viewAttrs.getFloat(R.styleable.ParallaxViewPager_parallaxPercent, 0);
    }

    // Offset is between 0 and 1 - represents percentage
    private Rect calculateSrc(int position, float offsetForCurrentPage){

        int pageCount = getAdapter().getCount();
        float totalOffset = offsetForCurrentPage + position;
        int eachPageMovesBackgroundBy = (mParallaxPixels / (pageCount - 1));

        if(src == null){
            src = new Rect();
        }

        src.left = (int) (totalOffset * eachPageMovesBackgroundBy);
        src.right = (mBackgroundBitmap.getWidth() + src.left) - (eachPageMovesBackgroundBy * (pageCount - 1));
        src.bottom = mBackgroundBitmap.getHeight();
        src.top = 0;

        return src;
    }

}
