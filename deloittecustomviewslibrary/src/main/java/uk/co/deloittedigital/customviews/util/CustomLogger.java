package uk.co.deloittedigital.customviews.util;

import android.util.Log;

/**
 * Custom Log class for logcat statements
 */
public final class CustomLogger {
    private String mClassName;

    public CustomLogger(boolean logInReleaseVariant, String className) {
        mClassName = className;
    }

    public void d(String msg) {
        Log.d(mClassName, msg);
    }

    public void w(String msg) {
        Log.w(mClassName, msg);
    }

    public void w(String msg, Throwable tr) {
        Log.w(mClassName, msg, tr);
    }

    public void i(String msg) {
        Log.i(mClassName, msg);
    }

    public void e(String msg) {
        Log.e(mClassName, msg);
    }

    public void e(String msg, Throwable tr) {
        Log.e(mClassName, msg, tr);
    }
}
