package uk.co.deloittedigital.customviews.animation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Animation for resizing a view over a set duration of time.
 */
public class ResizeView extends Animation {

    private View mView;
    private int mInitialHeight, mDeltaHeight;
    private int mInitialWidth, mDeltaWidth;

    public ResizeView(View v) {
        mView = v;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        mView.getLayoutParams().height = (int) (mInitialHeight + (mDeltaHeight * interpolatedTime));
        if (mDeltaWidth != 0) {
            mView.getLayoutParams().width = (int) (mInitialWidth + (mDeltaWidth * interpolatedTime));
        }
        mView.requestLayout();
    }

	/**
     * Set the initial width and the final width of the view
     * @param initialWidth Initial width of the view in px
     * @param endWidth Final width of the view in px
     */
    public void setWidth(int initialWidth, int endWidth) {
        mInitialWidth = initialWidth;
        mDeltaWidth = endWidth - initialWidth;
    }

	/**
     * Set the initial height and final height of the view
     * @param initialHeight Initial height of the view in px
     * @param endHeight Final height of the view in px
     */
    public void setHeight(int initialHeight, int endHeight) {
        mInitialHeight = initialHeight;
        mDeltaHeight = endHeight - initialHeight;
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
