package uk.co.deloittedigital.customviews.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.transition.ArcMotion;
import android.transition.TransitionSet;
import android.view.View;

import uk.co.deloittedigital.customviews.transition.MorphTransition;

/**
 * Utility class containing helper methods to ensure custom transitions are setup correctly.
 *
 * Created by kekwok on 13/12/2016.
 */

public class TransitionUtils {

	/**
	 * Updates the intent with the original view's background colour for the morph transition.
	 * @param intent Intent for starting a new Activity.
	 * @param viewBgColour Original view's background colour.
	 * @return Updated intent for starting the new Activity.
	 */
	public static Intent setupIntentExtrasForMorphTransition(@NonNull Intent intent, @ColorInt int viewBgColour) {
		intent.putExtra(MorphTransition.START_VIEW_BACKGROUND_COLOR, viewBgColour);
		return intent;
	}

	/**
	 * Sets up the morph transition when a new Activity is started. Invoke this from the new Activity
	 * in the {@link Activity#onCreate(Bundle)} method.
	 *
	 * The morph transition is applied on the target view when the Activity is created and also when
	 * it is finished. Upon finishing, the morph transition effects are applied in reverse.
	 *
	 * @param activity The new Activity which the transition is to be run on.
	 * @param intent Intent passed by the origin Activity.
	 * @param endViewBgColour Background colour of the target view.
	 * @param targetView The target View which the morph animation will be applied to.
	 * @param animationDuration The duration of the morph animation.
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public static void setupMorphTransitionBetweenActivities(@NonNull Activity activity,
	                                                         @NonNull Intent intent,
	                                                         @ColorInt int endViewBgColour,
	                                                         @NonNull View targetView,
	                                                         int animationDuration) {
		int initialViewBgColour = intent.getIntExtra(MorphTransition.START_VIEW_BACKGROUND_COLOR, 0);

		TransitionSet enterTransitionSet = createTransitionSet(initialViewBgColour, endViewBgColour, animationDuration);
		TransitionSet exitTransitionSet = createTransitionSet(endViewBgColour, initialViewBgColour, animationDuration);

		enterTransitionSet.addTarget(targetView);
		exitTransitionSet.addTarget(targetView);

		activity.getWindow().setSharedElementEnterTransition(enterTransitionSet);
		activity.getWindow().setSharedElementReturnTransition(exitTransitionSet);
	}

	/**
	 * Helper method to create the transition set which is applied on entering and returning from
	 * the Activity.
	 * @param initialViewBgColour
	 * @param endViewBgColour
	 * @param animationDuration
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private static TransitionSet createTransitionSet(@ColorInt int initialViewBgColour, @ColorInt int endViewBgColour, int animationDuration) {
		TransitionSet transitionSet = new TransitionSet();

		MorphTransition morphTransition = new MorphTransition(initialViewBgColour, endViewBgColour, animationDuration);
		ArcMotion arcMotion = new ArcMotion();
		arcMotion.setMinimumHorizontalAngle(50f);
		arcMotion.setMinimumVerticalAngle(50f);

		transitionSet.addTransition(morphTransition);
		transitionSet.setPathMotion(arcMotion);

		return transitionSet;
	}
}
