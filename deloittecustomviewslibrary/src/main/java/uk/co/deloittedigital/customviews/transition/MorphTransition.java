package uk.co.deloittedigital.customviews.transition;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.transition.ChangeBounds;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;

/**
 * Custom transition which takes two views and creates a morph animation between them.
 *
 * Created by kekwok on 09/12/2016.
 */

public class MorphTransition extends ChangeBounds {

	public static final String START_VIEW_BACKGROUND_COLOR = "deloittecustomviewslibrary:bgcolour";

	private int mStartBgColour, mEndBgColour;

	/**
	 * Sets up the morph transition by morphing between the shape of the two views as well background
	 * colours.
	 * @param startBgColour Background colour of the initial view.
	 * @param endBgColour Background colour of the target view.
	 * @param animationDuration Duration of the morph animation.
	 */
	public MorphTransition(@ColorInt int startBgColour, @ColorInt int endBgColour, int animationDuration) {
		mStartBgColour = startBgColour;
		mEndBgColour = endBgColour;
		setDuration(animationDuration);
	}

	/**
	 * This method is called by the transition system and is not intended the be called from external
	 * classes.
	 *
	 * The custom transition is defined here.
	 *
	 * @param sceneRoot
	 * @param startValues
	 * @param endValues
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	@Override
	public Animator createAnimator(final ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
		if (startValues == null || endValues == null) {
			// don't do any animation
			return null;
		}

		final View endView = endValues.view;
		final ValueAnimator bgAnimator = ValueAnimator.ofArgb(mStartBgColour, mEndBgColour);
		bgAnimator.setDuration(getDuration());
		bgAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animator) {
				endView.setBackgroundColor((int) animator.getAnimatedValue());
			}
		});

		// fade contents in dialog in after morph transition is complete
		final ViewGroup endViewGroup = (ViewGroup) endValues.view;
		for (int i = 0; i < endViewGroup.getChildCount(); i++) {
			View v = endViewGroup.getChildAt(i);
			v.setAlpha(0f);
			v.animate()
					.alpha(1f)
					.translationY(0f)
					.setStartDelay(getDuration())
					.setDuration(300);
		}

		final Animator animator = super.createAnimator(sceneRoot, startValues, endValues);
		animator.setDuration(getDuration());

		final AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.playTogether(animator, bgAnimator);
		animatorSet.setDuration(getDuration());
		return animatorSet;
	}
}
