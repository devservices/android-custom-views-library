package uk.co.deloittedigital.sample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import uk.co.deloittedigital.kekwok.sample.R;

public class ParallaxViewPagerFragment extends Fragment {

    private static final String ARTICLE_TITLE = "title";
    private static final String ARTICLE_SUBHEADING = "subheading";
    private static final String ARTICLE_CONTENT = "content";

    private View mRootView;

    private String mArticleTitle;
    private String mArticleSubheading;
    private String mArticleContent;

    public static ParallaxViewPagerFragment newInstance(Article article){
        Bundle arguments = new Bundle();
        arguments.putString(ARTICLE_TITLE, article.getTitle());
        arguments.putString(ARTICLE_SUBHEADING, article.getSubheading());
        arguments.putString(ARTICLE_CONTENT, article.getContent());
        ParallaxViewPagerFragment fragment = new ParallaxViewPagerFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mArticleTitle = getArguments().getString(ARTICLE_TITLE);
        mArticleSubheading = getArguments().getString(ARTICLE_SUBHEADING);
        mArticleContent = getArguments().getString(ARTICLE_CONTENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_parallaxviewpager_page, container, false);
        bindViews();
        return mRootView;
    }

    private void bindViews(){
        TextView mTitleTextView = (TextView) mRootView.findViewById(R.id.title);
        TextView mSubheadingTextView = (TextView) mRootView.findViewById(R.id.subheading);
        TextView mContentTextView = (TextView) mRootView.findViewById(R.id.content);

        mTitleTextView.setText(mArticleTitle);
        mSubheadingTextView.setText(mArticleSubheading);
        mContentTextView.setText(Html.fromHtml(mArticleContent));
    }

}
