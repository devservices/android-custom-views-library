package uk.co.deloittedigital.sample.nonlibdemo;

import android.view.View;

import uk.co.deloittedigital.customviews.animatedrecyclerview.AnimatedRecyclerViewHolder;

public class BaseViewHolder extends AnimatedRecyclerViewHolder {

    public BaseViewHolder(View view, View rootView) {
        super(view, rootView);
    }
}
