package uk.co.deloittedigital.sample.nonlibdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.sample.interfaces.ImageClickListener;

public class SharedTransitionFocusedFragment extends Fragment {

    private ImageClickListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        Best to set the enter/return transitions where you create the fragment rather than onCreate().
        This is an observation from many trial and error attempts
         */

//        setSharedElementEnterTransition(SharedTransitionsDemoActivity.getSharedElementTransition());
//        setSharedElementReturnTransition(SharedTransitionsDemoActivity.getSharedElementTransition());

//        setReenterTransition(SharedTransitionsDemoActivity.getNonSharedElementTransition());
//        setExitTransition(SharedTransitionsDemoActivity.getNonSharedElementTransition());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_marshmallow_layout_2, container, false);

        // This must be done to ensure the transition name is UNIQUE
        if (getArguments() != null && getArguments().getSerializable("image") != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);

            SharedTransitionsDemoActivity.Item item = (SharedTransitionsDemoActivity.Item) getArguments().getSerializable("image");
            imageView.setTransitionName(SharedTransitionsDemoActivity.TRANSITION_PREFIX + item);

            switch (item) {
                case BITCOIN: imageView.setImageResource(R.drawable.bitcoin); break;
                case CHROME: imageView.setImageResource(R.drawable.chrome); break;
                case TFL: imageView.setImageResource(R.drawable.tfl); break;
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStackImmediate();
                }
            });
        }

        return view;
    }

    public void setListener(ImageClickListener listener) {
        mListener = listener;
    }
}
