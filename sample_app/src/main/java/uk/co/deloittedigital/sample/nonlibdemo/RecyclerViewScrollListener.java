package uk.co.deloittedigital.sample.nonlibdemo;

public interface RecyclerViewScrollListener {
    void scrolledToRecyclerViewBottom(int lastIndex);
    void scrolledToRecyclerViewTop();
}
