package uk.co.deloittedigital.sample.nonlibdemo;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.customviews.util.CustomLogger;

// TODO Don't extend activity, extend a view
public abstract class BaseParallaxListActivity extends AppCompatActivity {
    private ImageView mImageView;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private TextView mTitleTextView;
    private FloatingActionButton mFab, mLowerRightFab;
    private RecyclerView mRecyclerView;

    private RecyclerViewScrollListener mRecyclerViewScrollListener;
    private boolean mIsLowerRightFabSet;

    private CustomLogger mLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parallax_list_activity);

        mLog = new CustomLogger(true, getClass().getSimpleName());

        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_container);
        mImageView = (ImageView) findViewById(R.id.imageview_toolbar);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitleTextView = (TextView) findViewById(R.id.title_textview);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mLowerRightFab = (FloatingActionButton) findViewById(R.id.lower_right_fab);
        mRecyclerView = (RecyclerView) findViewById(R.id.list);

        mIsLowerRightFabSet = false;
        setSupportActionBar(mToolbar);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mIsLowerRightFabSet) {
                    if (dy > 0) {
                        mLowerRightFab.hide();
                    } else if (dy < 0) {
                        mLowerRightFab.show();
                    }
                }

                if (!recyclerView.canScrollVertically(-1)) {
                    onScrolledToTop();
                } else if (!recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom();
                }
            }
        });
    }

    protected void setParallaxListHeaderImage(int resId) {
        mImageView.setImageResource(resId);
    }

    /**
     * Sets the background colour of the parallax header when the header is collapsed.
     * @param resource
     */
    protected void setParallaxListHeaderContentScrimColour(int resource) {
        mCollapsingToolbarLayout.setContentScrimColor(resource);
    }

    protected void setTitle(String title) {
        mCollapsingToolbarLayout.setTitleEnabled(true);
        mCollapsingToolbarLayout.setTitle(title);
    }

    /*
    This is required as there is a bug with CollapsingToolbarLayout where if no collapsible content
    is provided, the title will not show up. We workaround this bug by toggling a textview to become
    visible if this is the case.
     */
    protected void checkTitle() {
        if (mImageView.getDrawable() == null && mCollapsingToolbarLayout.getTitle() != null) {
            mTitleTextView.setText(mCollapsingToolbarLayout.getTitle().toString());
            mTitleTextView.setVisibility(View.VISIBLE);
        }
    }

    protected void setFloatingActionButtonImage(int drawableResId, View.OnClickListener listener) {
        mFab.setImageResource(drawableResId);
        mFab.setVisibility(View.VISIBLE);

        if (listener != null) {
            mFab.setClickable(true);
            mFab.setOnClickListener(listener);
        }
    }

    protected void setFloatingActionButtonBackgroundColour(int colour) {
        mFab.setBackgroundTintList(ColorStateList.valueOf(colour));
    }

    public final RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public final ImageView getImageView() {
        return mImageView;
    }

    public final CollapsingToolbarLayout getCollapsingToolbarLayout() {
        return mCollapsingToolbarLayout;
    }

    public final Toolbar getToolbar() {
        return mToolbar;
    }

    public final TextView getTitleTextView() {
        return mTitleTextView;
    }

    public final FloatingActionButton getFab() {
        return mFab;
    }

    public boolean getFabVisibilityStatus() {
        return mLowerRightFab.getVisibility() == View.VISIBLE;
    }

    public void setRecyclerViewScrollListener(RecyclerViewScrollListener listener) {
        mRecyclerViewScrollListener = listener;
    }

    protected void displayFab(int drawableResId, int backgroundColour, View.OnClickListener listener) {
        mIsLowerRightFabSet = true;
        mLowerRightFab.setImageResource(drawableResId);
        mLowerRightFab.setBackgroundColor(backgroundColour);
        mLowerRightFab.setOnClickListener(listener);
        mLowerRightFab.setVisibility(View.VISIBLE);
    }

    /**
     * Override this method to add the menu option to the toolbar. This will be pinned even when the
     * header image is fully visible in parallax mode.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    protected void onScrolledToBottom() {
        mLog.d("Scrolled to bottom of list");
        if (mRecyclerViewScrollListener != null) {
            mRecyclerViewScrollListener.scrolledToRecyclerViewBottom(mRecyclerView.getAdapter().getItemCount() - 1);
        }
    }

    protected void onScrolledToTop() {
        mLog.d("Scrolled to top of list");
        if (mRecyclerViewScrollListener != null) {
            mRecyclerViewScrollListener.scrolledToRecyclerViewTop();
        }
    }
}