package uk.co.deloittedigital.sample.nonlibdemo;

import android.app.Activity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.customviews.animation.AnimationUtil;
import uk.co.deloittedigital.sample.interfaces.ImageClickListener;

public class SharedTransitionsDemoActivity extends Activity implements ImageClickListener {

    public static final String TRANSITION_PREFIX = "image";

    public enum Item {
        CHROME, TFL, BITCOIN
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marshmallow);

        SharedTransitionInitialFragment fragment = new SharedTransitionInitialFragment();
        fragment.setListener(this);

        getFragmentManager().beginTransaction().replace(R.id.scene_root, fragment).commit();

    }

    public void click(Item item, View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("image", item);

        SharedTransitionFocusedFragment fragment = new SharedTransitionFocusedFragment();
        fragment.setListener(this);
        fragment.setArguments(bundle);

        fragment.setSharedElementEnterTransition(AnimationUtil.getSharedElementTransition(300));
        fragment.setSharedElementReturnTransition(AnimationUtil.getSharedElementTransition(300));
        fragment.setExitTransition(getNonSharedElementTransition());
        fragment.setEnterTransition(getNonSharedElementTransition());

        getFragmentManager()
                .beginTransaction()
                .addSharedElement(view, TRANSITION_PREFIX + item)
                .replace(R.id.scene_root, fragment)
                .addToBackStack(null)
                .commit();
    }

    public static Transition getNonSharedElementTransition() {
        TransitionSet transitionSet = new TransitionSet();
        Explode explode = new Explode();
        explode.setDuration(800);

        transitionSet.addTransition(explode);

        return transitionSet;
    }
}
