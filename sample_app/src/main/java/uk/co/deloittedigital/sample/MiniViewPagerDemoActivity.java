package uk.co.deloittedigital.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.customviews.adapter.MiniViewPagerAdapter;
import uk.co.deloittedigital.customviews.view.MiniViewPagerContainer;

public class MiniViewPagerDemoActivity extends AppCompatActivity {

    private static final String TAG = MiniViewPagerAdapter.class.getSimpleName();

    @BindView(R.id.miniviewpagercontainer)
    MiniViewPagerContainer mMiniViewPagerContainer;

    @BindView(R.id.viewpagerdemo_submit_btn)
    Button mSubmit;

    @BindView(R.id.viewpagerdemo_selectedvalue_tv)
    TextView mSelectedValue;

    private List<Integer> mPagerValues;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miniviewpagerdemo);
        ButterKnife.bind(this);

        initPager();
        setButtonListener();
    }

    private void initPager(){
        mPagerValues = generateFakeData();
        mMiniViewPagerContainer.setPagerValues(mPagerValues);
    }

    private void setButtonListener(){
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewPager pager = mMiniViewPagerContainer.getViewPager();
                final int currentItemPosition = pager.getCurrentItem();
                final int selectedItem = mPagerValues.get(currentItemPosition);
                mSelectedValue.setText("Selected value: " + selectedItem);
            }
        });
    }

    private List<Integer> generateFakeData(){
        List<Integer> data = new ArrayList<>();
        for(int i = 0; i < 30; i++){
            data.add(i + 1);
        }
        data.add(1, 1243453);
        return data;
    }

}
