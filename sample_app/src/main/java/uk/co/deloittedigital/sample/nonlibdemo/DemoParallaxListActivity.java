package uk.co.deloittedigital.sample.nonlibdemo;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.sample.ListAdapter;

import java.util.ArrayList;


public class DemoParallaxListActivity extends BaseParallaxListActivity {

    private ArrayList<String> mSampleListData;
    private ListAdapter mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setParallaxListHeaderImage(R.drawable.android);
        setParallaxListHeaderContentScrimColour(getResources().getColor(R.color.colorPrimary));
        setFloatingActionButtonImage(android.R.drawable.ic_dialog_map, null);
        setFloatingActionButtonBackgroundColour(getResources().getColor(R.color.colorPrimaryDark));
        setTitle("Demo Parallax Activity");
        checkTitle();

        mSampleListData = new ArrayList<>();
        addListData(10);

        displayFab(android.R.drawable.ic_dialog_dialer,
                R.color.colorAccent,
                null);

        setRecyclerViewScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void scrolledToRecyclerViewBottom(int lastIndex) {
                // we are simulating a network delay in getting new items and adding them to the list
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        addListData(5);
                        mListAdapter.updateData(mSampleListData);
                    }
                }, 2000);
            }

            @Override
            public void scrolledToRecyclerViewTop() {

            }
        });

        getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        mListAdapter = new ListAdapter(this, mSampleListData, getRecyclerView());
        getRecyclerView().setAdapter(mListAdapter);

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    private void addListData(int number) {
        for (int i = 0; i < number; i++) {
            String string = "Item " + (i+1);
            mSampleListData.add(string);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
