package uk.co.deloittedigital.sample.nonlibdemo;

import android.view.View;
import android.widget.ProgressBar;

import uk.co.deloittedigital.kekwok.sample.R;

public class ProgressBarViewHolder extends BaseViewHolder {
    protected ProgressBar mProgressBar;

    public ProgressBarViewHolder(View view) {
        super(view, view.findViewById(R.id.vh_container));
        mProgressBar = (ProgressBar) view.findViewById(R.id.vh_progressbar);
    }
}
