package uk.co.deloittedigital.sample;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.customviews.view.ParallaxViewPager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import uk.co.deloittedigital.customviews.util.CustomLogger;

public class ParallaxViewPagerDemoActivity extends AppCompatActivity {

    private static final String TAG = ParallaxViewPagerDemoActivity.class.getSimpleName();

    private ParallaxViewPager mViewPager;
    private CustomLogger mLog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLog = new CustomLogger(true, getClass().getSimpleName());
        setContentView(R.layout.activity_parallaxviewpagerdemo);
        mViewPager = (ParallaxViewPager) findViewById(R.id.pager);
        initViewPager();
    }

    private List<Article> createDemoArticleList(){
        AssetManager assetManager = getResources().getAssets();
        InputStream ims = null;
        try {
            ims = assetManager.open("articledata.json");
        } catch (IOException e) {
            mLog.e(e.getMessage(), e);
        }

        Gson gson = new Gson();
        Reader reader = new InputStreamReader(ims);

        Type listType = new TypeToken<List<Article>>(){}.getType();
        return gson.fromJson(reader, listType);
    }

    private void initViewPager(){
        PagerAdapter pagerAdapter = initViewPagerAdapter();
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(256, 20, 256, 20);
        mViewPager.setPageMargin(32);
        mViewPager.setAdapter(pagerAdapter);
    }

    private PagerAdapter initViewPagerAdapter(){
        List<Article> articles = createDemoArticleList();
        PagerAdapter pagerAdapter = new MyAdapter(getSupportFragmentManager(), articles);
        return pagerAdapter;
    }

    public static class MyAdapter extends FragmentStatePagerAdapter {

        private List<Article> mArticles;

        public MyAdapter(FragmentManager fm, List<Article> articles) {
            super(fm);
            this.mArticles = articles;
        }

        @Override
        public int getCount() {
            return mArticles.size();
        }

        @Override
        public Fragment getItem(int position) {
            return ParallaxViewPagerFragment.newInstance(mArticles.get(position));
        }
    }
}
