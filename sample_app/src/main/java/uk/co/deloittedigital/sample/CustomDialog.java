package uk.co.deloittedigital.sample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.deloittedigital.customviews.util.TransitionUtils;
import uk.co.deloittedigital.kekwok.sample.R;

/**
 * This is a custom dialog which extends from the Activity class, in order to demo the morph transition
 * effect. The morph transition effect can only be applied on Activities and does not work on the
 * native Dialog class, due to platform limitations.
 *
 * Created by kekwok on 09/12/2016.
 */

public class CustomDialog extends Activity {

	@BindView(R.id.dialog)
	LinearLayout mDialogLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.some_dialog);
		ButterKnife.bind(this);

		TransitionUtils.setupMorphTransitionBetweenActivities(this, getIntent(), getResources().getColor(R.color.white), mDialogLayout, 500);
	}

	@OnClick(R.id.dismiss_button)
	public void onClickDismissButton() {
		finishAfterTransition();
	}
}
