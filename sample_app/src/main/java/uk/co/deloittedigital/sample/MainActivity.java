package uk.co.deloittedigital.sample;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.deloittedigital.customviews.animation.AnimationUtil;
import uk.co.deloittedigital.customviews.util.TransitionUtils;
import uk.co.deloittedigital.customviews.view.ExpandVerticalView;
import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.sample.nonlibdemo.DemoParallaxListActivity;
import uk.co.deloittedigital.sample.nonlibdemo.SharedTransitionsDemoActivity;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.expand_vertical_view)
    ExpandVerticalView mExpandVerticalView;

    @BindView(R.id.reveal_layout)
    LinearLayout mCircularContainer;

    @BindView(R.id.crossfade_content)
    TextView mFillerTextView;

    @BindView(R.id.crossfade_loader)
    ProgressBar mFillerProgressBar;

    @BindView(R.id.scale_content)
    TextView mScaleContentTextView;

    @BindView(R.id.transition_content)
    TextView mTransitionContentTextView;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.viewpager_button)
    public void onClickViewPagerButton() {
        Intent intent = new Intent(MainActivity.this, MiniViewPagerDemoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.parallaxviewpager_button)
    public void onClickParallaxPagerButton() {
        Intent intent = new Intent(MainActivity.this, ParallaxViewPagerDemoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.expand_button)
    public void onClickExpandButton() {
        mExpandVerticalView.toggle();
    }

    @OnClick(R.id.circular_hide_button)
    public void onClickCircularHideButton() {
        AnimationUtil.circularHide(mCircularContainer, null);
    }

    @OnClick(R.id.circular_show_button)
    public void onClickCircularRevealButton() {
        AnimationUtil.circularReveal(mCircularContainer, null);
    }

    @OnClick(R.id.launch_list_activity_button)
    public void onClickLaunchListActivityButton() {
        Intent intent = new Intent(MainActivity.this, ListActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @OnClick(R.id.launch_parallax_list_activity_button)
    public void onClickLaunchParallaxListActivityButton() {
        Intent intent = new Intent(MainActivity.this, DemoParallaxListActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @OnClick(R.id.launch_shared_transitions_demo_activity)
    public void onClickLaunchSharedTransitionsDemoActivityButton() {
        Intent intent = new Intent(MainActivity.this, SharedTransitionsDemoActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @OnClick(R.id.toggle_crossfade_button)
    public void onClickToggleCrossfadeButton(final Button button) {
        if (mFillerProgressBar.getVisibility() == View.GONE) {
            AnimationUtil.crossfadeView(mFillerProgressBar, mFillerTextView);
            AnimationUtil.animateViewResize(button, button.getHeight(), 420, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    button.setText("Show Content");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        } else {
            AnimationUtil.crossfadeView(mFillerTextView, mFillerProgressBar);
            AnimationUtil.animateViewResize(button, button.getHeight(), 600, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    button.setText("Show Progress Spinner");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
    }

    @OnClick(R.id.toggle_scale_button)
    public void onClickToggleScaleContentButton (final Button button) {
        if (mScaleContentTextView.getVisibility() == View.VISIBLE) {
            AnimationUtil.animateViewOutAndScale(mScaleContentTextView, true, new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    button.setText(getText(R.string.animate_and_scale_text));
                }
            });
        } else {
            AnimationUtil.animateViewInAndScale(mScaleContentTextView, mScaleContentTextView.getWidth(), 500, null);
            button.setText(getText(R.string.hide_text));
        }
    }

    @OnClick(R.id.toggle_transition_button)
    public void onClickTransitionContentButton(final Button button) {
        if (mTransitionContentTextView.getVisibility() == View.VISIBLE) {
            AnimationUtil.animateViewOutToLeft(mTransitionContentTextView, mContext, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) { }

                @Override
                public void onAnimationEnd(Animation animation) {
                    button.setText(getText(R.string.animate_from_left_text));
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }
            });
        } else {
            AnimationUtil.animateViewInFromLeft(mTransitionContentTextView, mContext, mTransitionContentTextView.getWidth(), 500);
            button.setText(getText(R.string.hide_text));
        }
    }

    @OnClick(R.id.launch_dialog)
    public void onClickLaunchDialogButton(View view) {
        launchDialogWithMorphTransition(view);
    }

    @OnClick(R.id.activity_main_fab)
    public void onClickFab(View view) {
        launchDialogWithMorphTransition(view);
    }

    private void launchDialogWithMorphTransition(View view) {
        int colour = getResources().getColor(R.color.colorPrimary);

        if (view.getId() == R.id.activity_main_fab) {
            colour = getResources().getColor(R.color.colorAccent);
        }

        Intent intent = new Intent(MainActivity.this, CustomDialog.class);
        TransitionUtils.setupIntentExtrasForMorphTransition(intent, colour);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, view, "test");
        startActivity(intent, options.toBundle());
    }
}
