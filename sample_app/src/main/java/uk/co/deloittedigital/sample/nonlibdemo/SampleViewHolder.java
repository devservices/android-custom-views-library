package uk.co.deloittedigital.sample.nonlibdemo;

import android.view.View;
import android.widget.TextView;

import uk.co.deloittedigital.kekwok.sample.R;

public class SampleViewHolder extends BaseViewHolder {
    protected TextView textView;

    public SampleViewHolder(View view) {
        super(view, view.findViewById(R.id.vh_container));
        this.textView = (TextView) view.findViewById(R.id.vh_textview);
    }

    public TextView getTextView() {
        return textView;
    }
}
