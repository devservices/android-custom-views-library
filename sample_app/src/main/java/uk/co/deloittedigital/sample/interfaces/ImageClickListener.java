package uk.co.deloittedigital.sample.interfaces;

import android.view.View;
import uk.co.deloittedigital.sample.nonlibdemo.SharedTransitionsDemoActivity;

public interface ImageClickListener {
    void click(SharedTransitionsDemoActivity.Item item, View view);
}
