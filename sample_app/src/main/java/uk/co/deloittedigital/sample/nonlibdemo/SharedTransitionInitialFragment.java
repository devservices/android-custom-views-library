package uk.co.deloittedigital.sample.nonlibdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import uk.co.deloittedigital.kekwok.sample.R;
import uk.co.deloittedigital.customviews.animation.AnimationUtil;
import uk.co.deloittedigital.sample.interfaces.ImageClickListener;

public class SharedTransitionInitialFragment extends Fragment {

    private ImageView mChromeImage, mTflImage, mBitcoinImage;
    private ImageClickListener mListener;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SharedTransitionsDemoActivity.Item item;

            switch (view.getId()) {
                case R.id.chrome_imageview:
                    item = SharedTransitionsDemoActivity.Item.CHROME;
                    break;
                case R.id.bitcoin_imageview:
                    item = SharedTransitionsDemoActivity.Item.BITCOIN;
                    break;
                case R.id.tfl_imageview:
                    item = SharedTransitionsDemoActivity.Item.TFL;
                    break;
                default: item = SharedTransitionsDemoActivity.Item.BITCOIN;
            }

            mListener.click(item, view);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_marshmallow_layout_1, container, false);

        setSharedElementEnterTransition(AnimationUtil.getSharedElementTransition(300));
        setSharedElementReturnTransition(AnimationUtil.getSharedElementTransition(300));
        setExitTransition(SharedTransitionsDemoActivity.getNonSharedElementTransition());
        setEnterTransition(SharedTransitionsDemoActivity.getNonSharedElementTransition());

        mChromeImage = (ImageView) view.findViewById(R.id.chrome_imageview);
        mTflImage = (ImageView) view.findViewById(R.id.tfl_imageview);
        mBitcoinImage = (ImageView) view.findViewById(R.id.bitcoin_imageview);

        mBitcoinImage.setTransitionName(SharedTransitionsDemoActivity.TRANSITION_PREFIX + SharedTransitionsDemoActivity.Item.BITCOIN);
        mChromeImage.setTransitionName(SharedTransitionsDemoActivity.TRANSITION_PREFIX + SharedTransitionsDemoActivity.Item.CHROME);
        mTflImage.setTransitionName(SharedTransitionsDemoActivity.TRANSITION_PREFIX + SharedTransitionsDemoActivity.Item.TFL);

        mChromeImage.setOnClickListener(mOnClickListener);
        mTflImage.setOnClickListener(mOnClickListener);
        mBitcoinImage.setOnClickListener(mOnClickListener);

        return view;
    }

    public void setListener(ImageClickListener listener) {
        mListener = listener;
    }
}
