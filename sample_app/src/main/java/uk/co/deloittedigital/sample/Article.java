package uk.co.deloittedigital.sample;

public class Article {

    private String title;
    private String subheading;
    private String content;

    public String getTitle(){
        return this.title;
    }

    public String getSubheading(){
        return this.subheading;
    }

    public String getContent(){
        return this.content;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setSubheading(String subheading){
        this.subheading = subheading;
    }

    public void setContent(String content){
        this.content = content;
    }
}
